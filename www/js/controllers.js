angular.module('app.controllers', [])
  
.controller('messageCtrl', function($scope, $http) {
	$scope.data = {};
	$scope.data.sticker = 'https://upload.wikimedia.org/wikipedia/commons/3/33/Twemoji_1f602.svg';
	$scope.data.message = '';


	$scope.submitMessage = function() {
		var link = "http://wapley.com/show.php";	
	

		$http.post(link, $scope.data).then(function (res){
	        $scope.response = res.data;
	        console.log($scope.response);
	        $scope.data = {};
	    	$scope.data.serverResponse = "Sent";
	    });
	};

	$scope.setSticker = function(sticker) {
		switch (sticker) {
			case 1:
				$scope.data.sticker = "https://upload.wikimedia.org/wikipedia/commons/3/33/Twemoji_1f602.svg";
				break;
			case 2:
				$scope.data.sticker = "https://upload.wikimedia.org/wikipedia/commons/7/72/Twemoji_1f634.svg";
				break;
			case 3:
				$scope.data.sticker = "https://upload.wikimedia.org/wikipedia/commons/6/63/Twemoji_1f632.svg";
				break;
		}
	}
})
      
.controller('loginCtrl', function($scope, $state, $ionicHistory) {
	$scope.login = function() {
		$ionicHistory.nextViewOptions({
			disableBack: true
		});

		$state.go('menu.message')
	};
})
   
.controller('showsCtrl', function($scope) {

})
 