angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
      
        
    .state('menu.message', {
      url: '/message',
      views: {
        'side-menu21': {
          templateUrl: 'templates/message.html',
          controller: 'messageCtrl'
        }
      }
    })
        
      
    
      
    .state('menu', {
      url: '/options',
      abstract:true,
      templateUrl: 'templates/menu.html'
    })
      
    
      
        
    .state('menu.login', {
      url: '/login',
      views: {
        'side-menu21': {
          templateUrl: 'templates/login.html',
          controller: 'loginCtrl'
        }
      }
    })
        
      
    
      
        
    .state('menu.shows', {
      url: '/shows',
      views: {
        'side-menu21': {
          templateUrl: 'templates/shows.html',
          controller: 'showsCtrl'
        }
      }
    })
        
      
    ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/options/login');

});